<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'siteme' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':L>K`bOSUq-LHljdsX-u]J:uH`[dP:j~H5 UEXztl|UXvPXgGl^7L`xLDY(Pu@z^' );
define( 'SECURE_AUTH_KEY',  '*}G1r$,YIO{#riPTNb1PoY*~GB678DODTD:U4{f/C[PU9Kg/%MJ;Lu?Y &KC9fcu' );
define( 'LOGGED_IN_KEY',    '-zyI#?.mjSc_r&V{N^v]hZZf`w^Kq)L5jl]AXE12Td8P*e{W0!8A{aIZS|HoqY:p' );
define( 'NONCE_KEY',        '<o5PNDCTA4,P&6gVK6]J,^]2:CDGkTAh#jt!}3=O(}F$:U#2#!|&RrvR$#6--FTo' );
define( 'AUTH_SALT',        'ps$V60QfsNVUMf<WB1wza07hC/m<QAxd1H95GEW|k=6M+g!vDIFlNNdwHH{3k!5q' );
define( 'SECURE_AUTH_SALT', 'iqj7|h<WhBH1N!}HU#Xn6XWzf.nc&VdVn|!k#x&W,Bbk`-ZqPR`%o`$(6E;g|MyU' );
define( 'LOGGED_IN_SALT',   '-9HCoh$X<ur4:NXO&owWQ7r,P/5rtM>mg].efbI1Bf}eJPu>!G }vZ|1Gr.Xj.c ' );
define( 'NONCE_SALT',       'Xt!z|^T5l^+mIBoZ^X,mB+Q0{GRjWV+3.BD^hx^VHuU1+p#V(+OJSPYp.O5rRV]m' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
