<?php
/*
  Plugin Name: Ajout d'un texte sur une page single
  Description: Permet d'afficher une ligne de texte au début de chaque article.
  Version: 0.1
  Author: Bastien M.
  License: No License
*/

function hello_world($content){
    $text = "<p>Dev by StAwN for Wordpress Epicerie</p>";
    return $text.$content;
}

add_action('the_content', 'hello_world');
